drop table if exists ActeFacturer cascade;
drop table if exists DetailActeNiala cascade;
drop table if exists DetailActe cascade;
drop table if exists ActeNiala cascade;
drop table if exists Acte cascade;
drop table if exists RecetteNiala cascade;
drop table if exists Recette cascade;
drop table if exists Mois cascade;
drop table if exists DepenseNiala cascade;
drop table if exists Depense cascade;
drop table if exists TypeDepenseNiala cascade;
drop table if exists TypeDepense cascade;
drop table if exists TypeActeNiala cascade;
drop table if exists TypeActe cascade;
drop table if exists PatientNiala cascade;
drop table if exists Patient cascade;
drop table if exists Genre cascade;
drop table if exists Utilisateur cascade;


create table Utilisateur(
    idUtilisateur serial primary key,
    email varchar(255) not null unique,
    mdp varchar(255) not null,
    statut int not null
);
insert into Utilisateur(email,mdp,statut) values ('iamadmin@gmail.com','mdpadmin','0');
insert into Utilisateur(email,mdp,statut) values ('iamuser@gmail.com','mdpuser','1');

create table Genre(
    idGenre serial primary key,
    nomGenre varchar(255) not null unique
);
insert into Genre(nomGenre) values ('Homme');
insert into Genre(nomGenre) values ('Femme');

create table Patient(
    idPatient serial primary key,
    nom varchar(255) not null unique,
    dateNaissance date not null,
    idGenre int not null,
    foreign key(idGenre) references Genre(idGenre),
    remboursement boolean not null
);

create table PatientNiala(
    idPatientNiala serial primary key,
    idPatient int not null,
    foreign key(idPatient) references Patient(idPatient)
);

create or replace view v_patient as
    select p.*,nomgenre,extract(year from now())-extract(year from dateNaissance) as age from patient p join genre g on p.idgenre=g.idgenre where idPatient not in (select idPatient from PatientNiala);

create table TypeActe(
    idTypeActe serial primary key,
    nomTypeActe varchar(255) not null unique,
    budgetActeAnnuel decimal(10,2) not null,
    budgetActeMensuel decimal(10,2) not null,
    codeActe char(3) not null unique
);

create table TypeActeNiala(
    idTypeActeNiala serial primary key,
    idTypeActe int not null,
    foreign key(idTypeActe) references TypeActe(idTypeActe)
);

create or replace view v_TypeActe as
    select*from typeActe where idTypeActe not in(select idTypeActe from TypeActeNiala);

create table TypeDepense(
    idTypeDepense serial primary key,
    nomTypeDepense varchar(255) not null unique,
    budgetDepenseAnnuel decimal(10,2) not null,
    budgetDepenseMensuel decimal(10,2) not null,
    codeDepense char(3) not null unique
);

create table TypeDepenseNiala(
    idTypeDepenseNiala serial primary key,
    idTypeDepense int not null,
    foreign key(idTypeDepense) references TypeDepense(idTypeDepense)
);

create or replace view v_TypeDepense as
    select*from typeDepense where idTypeDepense not in(select idTypeDepense from TypeDepenseNiala);

create table Depense(
    idDepense serial primary key,
    dateDepense date not null,
    idTypeDepense int not null,
    foreign key(idTypeDepense) references TypeDepense(idTypeDepense),
    montantDepense decimal(10,2) not null
);

create table DepenseNiala(
    idDepenseNiala serial primary key,
    idDepense int not null,
    foreign key(idDepense) references Depense(idDepense)
);

create or replace view v_Depense as
    select d.*,nomtypedepense,extract(year from datedepense) as annee,extract(month from datedepense) as mois,codedepense,budgetdepenseannuel,budgetdepensemensuel from depense d join typeDepense td on d.idtypedepense=td.idtypedepense where iddepense not in(select iddepense from depenseniala) order by dateDepense asc;

create table Mois(
    idmois serial primary key,
    nommois varchar(100) not null unique
);
insert into Mois(nommois) values ('Janvier');
insert into Mois(nommois) values ('Février');
insert into Mois(nommois) values ('Mars');
insert into Mois(nommois) values ('Avril');
insert into Mois(nommois) values ('Mai');
insert into Mois(nommois) values ('Juin');
insert into Mois(nommois) values ('Juillet');
insert into Mois(nommois) values ('Août');
insert into Mois(nommois) values ('Septembre');
insert into Mois(nommois) values ('Octobre');
insert into Mois(nommois) values ('Novembre');
insert into Mois(nommois) values ('Décembre');

create or replace view anneeDepense as
    select annee from v_depense group by annee;

create or replace view anneemoisdepense as
select*from anneeDepense ad left join mois m on 1=1 left join typedepense td on 1=1 order by annee,idmois,idtypedepense asc;

create or replace view v_depenseMois as
select annee,mois,nomtypedepense,sum(montantdepense) as montantDepense from v_depense group by annee,mois,nomtypedepense order by annee,mois;

create or replace view v_depenseParMois as 
select an.*,coalesce(montantdepense,0) as montantdepense from anneemoisdepense an left join v_depenseMois tv on an.annee=tv.annee and an.idmois=tv.mois and an.nomtypedepense=tv.nomtypedepense;

create table Recette(
    idRecette serial primary key,
    dateRecette date not null,
    idTypeActe int not null,
    foreign key(idTypeActe) references TypeActe(idTypeActe),
    idPatient int not null,
    foreign key(idPatient) references Patient(idPatient),
    montantRecette decimal(10,2) not null
);

create table RecetteNiala(
    idRecetteNiala serial primary key,
    idRecette int not null,
    foreign key(idRecette) references Recette(idRecette)
);

create or replace view v_recette as
    select vp.*,ta.*,idrecette,daterecette,montantrecette,extract(year from daterecette) as annee,extract(month from daterecette) as mois from recette r join v_patient vp on r.idpatient=vp.idpatient join typeActe ta on r.idtypeacte=ta.idtypeacte where idrecette not in(select idrecette from recetteniala);

create or replace view anneeRecette as
    select annee from v_recette group by annee;

create or replace view anneemoisrecette as
select*from anneeRecette ad left join mois m on 1=1 left join typeacte td on 1=1 order by annee,idmois,idtypeacte asc;

create or replace view v_recetteMois as
select annee,mois,nomtypeacte,sum(montantrecette) as montantrecette from v_recette group by annee,mois,nomtypeacte order by annee,mois;

create or replace view v_recetteParMois as 
select an.*,coalesce(montantrecette,0) as montantrecette from anneemoisrecette an left join v_recetteMois tv on an.annee=tv.annee and an.idmois=tv.mois and an.nomtypeacte=tv.nomtypeacte;

create table acte(
    idActe serial primary key,
    dateActe date not null,
    idPatient int not null,
    foreign key(idPatient) references Patient(idPatient)
); 

create table ActeNiala(
    idActeNiala serial primary key,
    idActe int not null,
    foreign key(idActe) references Acte(idActe)
);

create or replace view v_acte as
    select a.*,nom,age from Acte a join v_patient p on a.idpatient=p.idpatient and idActe not in(select idActe from ActeNiala);

create table DetailActe(
    idDetailActe serial primary key,
    idActe int not null,
    foreign key(idActe) references Acte(idActe),
    idTypeActe int not null,
    foreign key(idTypeActe) references TypeActe(idTypeActe),
    montantActe decimal(10,2) not null,
    dateDetail date
);

create table DetailActeNiala(
    idDetailActeNiala serial primary key,
    idDetailActe int not null,
    foreign key(idDetailActe) references DetailActe(idDetailActe)
);

create table ActeFacturer(
    idActeFacturer serial primary key,
    idActe int not null unique,
    foreign key(idActe) references Acte(idActe),
    dateFacture date
);


create or replace view v_detailacte as
    select ta.*,iddetailacte,datedetail,montantacte,extract(year from dateDetail) as annee,extract(month from datedetail) as mois,idacte from detailacte r join typeActe ta on r.idtypeacte=ta.idtypeacte where iddetailacte not in(select iddetailacte from detailacteniala);

create or replace view anneeDetailActe as
    select annee from v_detailacte group by annee;

create or replace view anneemoisdetail as
select*from anneeDetailActe ad left join mois m on 1=1 left join typeacte td on 1=1 order by annee,idmois,idtypeacte asc;

create or replace view v_detailMois as
select annee,mois,nomtypeacte,sum(montantacte) as montantacte from v_detailacte group by annee,mois,nomtypeacte order by annee,mois;

create or replace view v_detailParMois as 
select an.*,coalesce(montantacte,0) as montantacte from anneemoisdetail an left join v_detailMois tv on an.annee=tv.annee and an.idmois=tv.mois and an.nomtypeacte=tv.nomtypeacte;
