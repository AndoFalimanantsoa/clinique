<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tables / Data - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Tableau de bord</h5>
              <form class="row g-3" method="get" action="<?php echo site_url('Admin_Controlleur/depenseMois'); ?>">
                <div class="col-md-2">
                  <input type="text" class="form-control" placeholder="Annee" name="annee" id="annee">
                </div>
                <div class="col-md-2">
                <select name="idmois" class="form-select">
                    <option value="">Mois</option>
                    <?php for ($i=0; $i <count($ListeMois) ; $i++) { ?>
                        <option value="<?php echo $ListeMois[$i]['idmois']; ?>"><?php echo $ListeMois[$i]['nommois']; ?></option>
                    <?php } ?>
                </select>
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary">Rechercher</button>
                </div>
              </form><!-- End No Labels Form -->
              <!-- Default Table -->
              <div>
              
              <table class="table" style="margin-top: 20px;">
                <thead>
                  <tr>
                    <th><h5 class="card-title">Recette</h5></th>
                  </tr>
                  <tr>
                  <th scope="col">Type Acte</th>
                  <th scope="col">Réel</th>
                    <th scope="col">Budget</th>
                    <th scope="col">Realisation</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $reelRecette=0;
                    $budgetRecette=0;
                   for ($i=0; $i <count($RecetteMois) ; $i++) {
                    $realisationR=($RecetteMois[$i]['montantacte']*100)/$RecetteMois[$i]['budgetactemensuel'];
                    $reelRecette=$reelRecette+$RecetteMois[$i]['montantacte'];
                    $budgetRecette=$budgetRecette+$RecetteMois[$i]['budgetactemensuel'];
                    $realisationRecette=(100*$reelRecette)/$budgetRecette;
                     ?>
                    <tr>
                      <td><?php echo $RecetteMois[$i]['nomtypeacte'] ?></td>
                      <td><?php echo $RecetteMois[$i]['montantacte'] ?></td>
                      <td><?php echo $RecetteMois[$i]['budgetactemensuel'] ?></td>
                      <td><?php echo sprintf("%.2f",$realisationR) ?>%</td>
                    </tr>
                  <?php } ?>
                    <tr>
                      <td></td>
                      <th scope="row"><?php echo $reelRecette ?></th>
                      <th scope="row"><?php echo $budgetRecette ?></th>
                      <th scope="row"><?php echo sprintf("%.2f",$realisationRecette) ?>%</th>
                    </tr>
                    <tr>
                      <th><h5 class="card-title">Depense</h5></th>
                    </tr>
                    <tr>
                  <th scope="col">Type Acte</th>
                  <th scope="col">Réel</th>
                    <th scope="col">Budget</th>
                    <th scope="col">Realisation</th>
                  </tr>
                      <?php
                      $reelDepense=0;
                      $budgetDepense=0;
                    for ($i=0; $i <count($DepenseMois) ; $i++) {
                      $realisation=($DepenseMois[$i]['montantdepense']*100)/$DepenseMois[$i]['budgetdepensemensuel'];
                      $reelDepense=$reelDepense+$DepenseMois[$i]['montantdepense'];
                      $budgetDepense=$budgetDepense+$DepenseMois[$i]['budgetdepensemensuel'];
                      $realisationDepense=(100*$reelDepense)/$budgetDepense;
                      ?>
                    <tr>
                      <td><?php echo $DepenseMois[$i]['nomtypedepense'] ?></td>
                      <td><?php echo $DepenseMois[$i]['montantdepense'] ?></td>
                      <td><?php echo $DepenseMois[$i]['budgetdepensemensuel'] ?></td>
                      <td><?php echo sprintf("%.2f",$realisation) ?>%</td>
                    </tr>
                  <?php } ?>
                    <tr>
                      <td></td>
                      <th scope="row"><?php echo $reelDepense ?></th>
                      <th scope="row"><?php echo $budgetDepense ?></th>
                      <th scope="row"><?php echo sprintf("%.2f",$realisationDepense) ?>%</th>
                    </tr>
                    <tr>
                      <th><h5 class="card-title">Bénéfice</h5></th>
                    </tr>
                    <tr>
                  <th scope="col"></th>
                  <th scope="col">Réel</th>
                    <th scope="col">Budget</th>
                    <th scope="col">Realisation</th>
                  </tr>
                    <?php 
                  $reelBenefice=$reelRecette-$reelDepense;
                  $budgetBenefice=$budgetRecette-$budgetDepense;
                  if($reelBenefice==0){
                    $realisationBenefice=0;
                  }
                  else{
                    $realisationBenefice=(100*$reelBenefice)/$budgetBenefice;
                  }
                ?>
                <tr>
                      <td>Recette</td>
                      <td><?php echo $reelRecette ?></td>
                      <td><?php echo $budgetRecette ?></td>
                      <td><?php echo sprintf("%.2f",$realisationRecette) ?>%</td>
                    </tr>
                    <tr>
                      <td>Dépense</td>
                      <td><?php echo $reelDepense ?></td>
                      <td><?php echo $budgetDepense ?></td>
                      <td><?php echo sprintf("%.2f",$realisationDepense) ?>%</td>
                    </tr>
                    <tr>
                      <td></td>
                      <th scope="row"><?php echo sprintf("%.2f",$reelBenefice) ?></th>
                      <th scope="row"><?php echo sprintf("%.2f",$budgetBenefice) ?></th>
                      <th scope="row"><?php echo sprintf("%.2f",$realisationBenefice) ?>%</th>
                    </tr>
                </tbody>
              </table>
            </div>
            </div>
          </div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/chart.js/chart.min.js"></script>
  <script src="assets/vendor/echarts/echarts.min.js"></script>
  <script src="assets/vendor/quill/quill.min.js"></script>
  <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>