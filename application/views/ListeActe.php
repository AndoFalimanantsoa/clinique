<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tables / Data - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Acte</h5>
              <!-- Default Table -->
              <form class="row g-3" method="get" action="<?php echo site_url('User_Controlleur/Acte'); ?>">
                <div class="col-md-2">
                  <input type="text" class="form-control" placeholder="Nom" name="nompatient">
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary">Rechercher</button>
                </div>
              </form><!-- End No Labels Form -->
              <div>
              <table class="table" style="margin-top: 20px;">
                <thead>
                  <tr>
                    <th scope="col">Patient</th>
                    <th scope="col">Date</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  for ($i=0; $i <count($ListeActe) ; $i++) { ?>
                  <tr>
                    <td><?php echo $ListeActe[$i]['nom'] ?></td>  
                    <td><?php echo $ListeActe[$i]['dateacte'] ?></td>
                    <td><a type="button" class="btn btn-primary" href="<?php echo site_url('User_Controlleur/DetailActe/'); ?>?idacte=<?php echo $ListeActe[$i]['idacte'] ?>"><i class="ri-file-text-line"></i></a></td>
                    <td>              
                        <!-- Basic Modal -->
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#basicModal<?php echo $i; ?>">
                        <i class="ri-edit-box-fill"></i>
                        </button>
                        <div class="modal fade" id="basicModal<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title">Modifier acte</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                <form method="get" action="<?php echo site_url('User_Controlleur/updateActe'); ?>">
                                <input type="hidden" name="idacte" value="<?php echo $ListeActe[$i]['idacte'] ?>">
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Patient</label>
                                    <div class="col-sm-10">
                                    <select name="idpatient" class="form-select">
                                    <?php for ($j=0; $j <count($ListePatient) ; $j++) { ?>
                                        <option value="<?php echo $ListePatient[$j]['idpatient']; ?>"><?php echo $ListePatient[$j]['nom']; ?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Date</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="dateacte" id="dateacte"  required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                                </div>
                                </div>
                                </form>                                
                                </div>
                            </div>
                            </div>
                        </div><!-- End Basic Modal--> 
                    </td>
                    <td>                
                                            <!-- Vertically centered Modal -->
                        <a type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i; ?>">
                        <i class="ri-delete-bin-5-fill"></i>
                            </a>
                        <div class="modal fade" id="verticalycentered<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <form methodd="get" action="<?php echo site_url('User_Controlleur/deleteActe'); ?>">
                                <div class="modal-body">
                                <input type="hidden" name="idacte" value="<?php echo $ListeActe[$i]['idacte'] ?>">
                                Etes-vous sûre de vouloir supprimer cette ligne?
                                </div>
                                <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Supprimer</button>
                                </div>
                            </form>
                            </div>
                            </div>
                        </div><!-- End Vertically centered Modal-->
                    </td> 
                  </tr>
                  <?php } ?>
                </tbody>
              </table>

                <!-- Vertically centered Modal -->
               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycentered">
                Ajouter
              </button>
              <div class="modal fade" id="verticalycentered" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Ajouter acte</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
               <!-- General Form Elements -->
               <form method="GET" action="<?php echo site_url('User_Controlleur/addActe'); ?>" >
                <div class="row mb-3">
                    <label for="inputText" class="col-sm-2 col-form-label">Patient</label>
                    <div class="col-sm-10">
                    <select name="idpatient" class="form-select">
                    <?php for ($i=0; $i <count($ListePatient) ; $i++) { ?>
                        <option value="<?php echo $ListePatient[$i]['idpatient']; ?>"><?php echo $ListePatient[$i]['nom']; ?></option>
                    <?php } ?>
                </select>
                    </div>
                </div>
               <div class="row mb-3">
                    <label for="inputText" class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="dateacte" id="dateacte" value="" required>
                    </div>
                </div>
                <div class="row mb-3">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;">Valider</button>
                  </div>
                </div>
              </form>
              <!-- End General Form Elements -->                   
              </div><!-- End Vertically centered Modal-->
            </div>
            </div>
          </div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/chart.js/chart.min.js"></script>
  <script src="assets/vendor/echarts/echarts.min.js"></script>
  <script src="assets/vendor/quill/quill.min.js"></script>
  <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>