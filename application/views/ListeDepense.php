<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tables / Data - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Dépense</h5>
              <!-- Default Table -->
              <form class="row g-3" method="get" action="<?php echo site_url('User_Controlleur/depense'); ?>">
                <div class="col-md-2">
                <select name="idtypedepense" class="form-select">
                    <option value="">Type</option>
                    <?php for ($i=0; $i <count($ListeTypeDepense) ; $i++) { ?>
                        <option value="<?php echo $ListeTypeDepense[$i]['idtypedepense']; ?>"><?php echo $ListeTypeDepense[$i]['nomtypedepense']; ?></option>
                    <?php } ?>
                </select>
                </div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary">Rechercher</button>
                </div>
              </form><!-- End No Labels Form -->
              <div>
              <table class="table" style="margin-top: 20px;">
                <thead>
                  <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Date</th>
                    <th scope="col">Montant</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  for ($i=0; $i <count($ListeDepense) ; $i++) { ?>
                  <tr>
                    <td><?php echo $ListeDepense[$i]['nomtypedepense'] ?></td>  
                    <td><?php echo $ListeDepense[$i]['datedepense'] ?></td>
                    <td><?php echo $ListeDepense[$i]['montantdepense'] ?>Ar</td>
                    <td>              
                        <!-- Basic Modal -->
                        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#basicModal<?php echo $i; ?>">
                        <i class="ri-edit-box-fill"></i>
                        </button>
                        <div class="modal fade" id="basicModal<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title">Modifier dépense</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                <form method="get" action="<?php echo site_url('User_Controlleur/updateDepense'); ?>">
                                <input type="hidden" name="iddepense" value="<?php echo $ListeDepense[$i]['iddepense'] ?>">
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-3 col-form-label">Type</label>
                                    <div class="col-md-9">
                                    <select name="idtypedepense" class="form-select">
                                    <?php for ($j=0; $j <count($ListeTypeDepense) ; $j++) { ?>
                                        <option value="<?php echo $ListeTypeDepense[$j]['idtypedepense']; ?>"><?php echo $ListeTypeDepense[$j]['nomtypedepense']; ?></option>
                                    <?php } ?>
                                    </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-3 col-form-label">Date</label>
                                    <div class="col-md-9">
                                        <input type="date" class="form-control" name="datedepense" id="datedepense"  required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-3 col-form-label">Montant</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="montantdepense" id="montantdepense" value="<?php echo $ListeDepense[$i]['montantdepense'] ?>">                                   
                                     </div>
                                </div>
                                <div class="row mb-3">
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-primary" style="width: 340px; margin-left:125px;">Valider</button>
                                </div>
                                </div>
                                </form>                                
                                </div>
                            </div>
                            </div>
                        </div><!-- End Basic Modal--> 
                    </td>
                    <td>                
                                            <!-- Vertically centered Modal -->
                        <a type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#verticalycentered<?php echo $i; ?>">
                        <i class="ri-delete-bin-5-fill"></i>
                            </a>
                        <div class="modal fade" id="verticalycentered<?php echo $i; ?>" tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <form methodd="get" action="<?php echo site_url('User_Controlleur/deleteDepense'); ?>">
                                <div class="modal-body">
                                <input type="hidden" name="iddepense" value="<?php echo $ListeDepense[$i]['iddepense'] ?>">
                                Etes-vous sûre de vouloir supprimer cette ligne?
                                </div>
                                <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Supprimer</button>
                                </div>
                            </form>
                            </div>
                            </div>
                        </div><!-- End Vertically centered Modal-->
                    </td> 
                  </tr>
                  <?php } ?>
                </tbody>
              </table>

                <!-- Vertically centered Modal -->
               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#verticalycentered">
                Ajouter
              </button>
              <div class="modal fade" id="verticalycentered" tabindex="-1">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Ajouter dépense</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
               <!-- General Form Elements -->
               <form method="GET" action="<?php echo site_url('User_Controlleur/addDepense'); ?>" >
                <div class="row mb-3">
                    <label for="inputText" class="col-sm-3 col-form-label">Type</label>
                    <div class="col-md-9">
                    <select name="idtypedepense" class="form-select">
                    <?php for ($i=0; $i <count($ListeTypeDepense) ; $i++) { ?>
                        <option value="<?php echo $ListeTypeDepense[$i]['idtypedepense']; ?>"><?php echo $ListeTypeDepense[$i]['nomtypedepense']; ?></option>
                    <?php } ?>
                </select>
                    </div>
                </div>
                <div class="row mb-3" >
                <label for="inputText" class="col-sm-3 col-form-label">Date</label>
                  <div class="col-md-3" style="margin-left: 0px">
                    <input type="text" class="form-control" placeholder="Jour" name="jour" id="jour">
                  </div
                  <legend class="col-form-label col-sm-2 pt-0"></legend>
                  <div class="col-md-2">
                    <?php for ($i=0; $i <count($ListeMois) ; $i++) {  ?> 
                    <div class="form-check" style="margin-left: 7px">
                      <input class="form-check-input" type="checkbox" id="gridCheck<?php echo $i ?>" value="<?php echo $ListeMois[$i]['idmois'] ?>" name="mois[]" >
                      <label class="form-check-label" for="gridCheck<?php echo $i ?>">
                        <?php echo $ListeMois[$i]['nommois'] ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                  <div class="col-md-3" style="margin-left: 38px">
                    <input type="text" class="form-control" placeholder="Annee" name="annee" id="annee" >
                  </div
                </div>
                <div class="row mb-3">
                    <label for="inputText" class="col-sm-3 col-form-label">Montant</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="montantdepense" id="montantdepense" value="" required>                                    </div>
                </div>
                <div class="row mb-3">
                  <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary" style="width: 340px; margin-left:125px;">Valider</button>
                  </div>
                </div>
              </form>
              <div class="modal-header">
                <h5 class="modal-title">Importer un fichier csv</h5>
              </div>
              <div class="modal-body">
              <form enctype="multipart/form-data" action="<?php echo site_url('User_Controlleur/csvToBase'); ?>" method="post">
                <div class="row mb-3">
                  <label for="inputNumber" class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-10">
                    <input class="form-control" type="file" id="file" accept=".csv" name="file">
                  </div>
                </div> 
                <div class="row mb-3">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary" style="width: 380px; margin-left:85px;" name="import">Importer</button>
                  </div>
                </div>   
              </form>
              </div>
              <!-- End General Form Elements -->                   
              </div><!-- End Vertically centered Modal-->
            </div>
            </div>
          </div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/chart.js/chart.min.js"></script>
  <script src="assets/vendor/echarts/echarts.min.js"></script>
  <script src="assets/vendor/quill/quill.min.js"></script>
  <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>