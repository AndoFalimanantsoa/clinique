<?php 	
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controlleur extends CI_Controller{
	public function __construct(){
        parent::__construct();
    }

    public function typeActe(){
        $this->load->model('Mon_Model');
        $data=array();
        $data['ListeTypeActe']=$this->Mon_Model->getAll("V_TypeActe");
        $data['page']="ListeTypeActe";
        $this->load->view('AccAdmin',$data);   
    }
    public function addTypeActe(){
        $nomtypeacte=$_GET['nomtypeacte'];
        $code=$_GET['code'];
        $budget=$_GET['budget'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->addTypeActe($nomtypeacte,$code,$budget);
        $this->typeActe();
    }
    public function updateTypeActe(){
        $nomtypeacte=$_GET['nomtypeacte'];
        $code=$_GET['code'];
        $budget=$_GET['budget'];
        $idtypeacte=$_GET['idtypeacte'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->updateTypeActe($nomtypeacte,$code,$budget,$idtypeacte);
        $this->typeActe();
    }
    public function deleteTypeActe(){
        $idtypeacte=$_GET['idtypeacte'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->deleteTypeActe($idtypeacte);
        $this->typeActe();
    }

    public function typeDepense(){
        $this->load->model('Mon_Model');
        $data=array();
        $data['ListeTypeDepense']=$this->Mon_Model->getAll("V_TypeDepense");
        $data['page']="ListeTypeDepense";
        $this->load->view('AccAdmin',$data);   
    }
    public function addTypeDepense(){
        $nomtypedepense=$_GET['nomtypedepense'];
        $code=$_GET['code'];
        $budget=$_GET['budget'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->addTypeDepense($nomtypedepense,$code,$budget);
        $this->typeDepense();
    }
    public function updateTypeDepense(){
        $nomtypedepense=$_GET['nomtypedepense'];
        $code=$_GET['code'];
        $budget=$_GET['budget'];
        $idtypedepense=$_GET['idtypedepense'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->updateTypeDepense($nomtypedepense,$code,$budget,$idtypedepense);
        $this->typeDepense();
    }
    public function deleteTypeDepense(){
        $idtypedepense=$_GET['idtypedepense'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->deleteTypeDepense($idtypedepense);
        $this->typeDepense();
    }

    public function patient(){
        $this->load->model('Mon_Model');
        $data=array();
        $data['ListeGenre']=$this->Mon_Model->getAll("Genre");
        $nom=isset($_GET['nom']) ? $_GET['nom'] : ""; 
        $age=isset($_GET['age']) ? $_GET['age'] : ""; 
        $idgenre=isset($_GET['idgenre']) ? $_GET['idgenre'] : ""; 
        $remboursement=isset($_GET['remboursement']) ? $_GET['remboursement'] : ""; 
        $this->load->model('Admin_Model');
        $data['ListePatient']=$this->Admin_Model->getPatient($nom,$age,$idgenre,$remboursement);
        $data['page']="ListePatient";
        $this->load->view('AccAdmin',$data);     
    }
    public function addPatient(){
        $nom=$_GET['nom'];
        $datenaissance=$_GET['datenaissance'];
        $idgenre=$_GET['idgenre'];
        $remboursement=$_GET['remboursement'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->addPatient($nom,$datenaissance,$idgenre,$remboursement);
        $this->patient();
    }
    public function updatePatient(){
        $nom=$_GET['nom'];
        $datenaissance=$_GET['datenaissance'];
        $idgenre=$_GET['idgenre'];
        $remboursement=$_GET['remboursement'];
        $idpatient=$_GET['idpatient'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->updatePatient($nom,$datenaissance,$idgenre,$remboursement,$idpatient);
        $this->patient();
    }
    public function deletePatient(){
        $idpatient=$_GET['idpatient'];
        $this->load->model('Admin_Model');
        $this->Admin_Model->deletePatient($idpatient);
        $this->patient();
    }
    public function depenseMois(){
        $annee=isset($_GET['annee']) ? $_GET['annee'] : "";
        $idmois=isset($_GET['idmois']) ? $_GET['idmois'] : "";
        $data=array();
        $this->load->model('Mon_Model'); 
        $data['ListeMois']=$this->Mon_Model->getAll("mois");
        $this->load->model('Admin_Model');     
        $data['DepenseMois']=$this->Admin_Model->getDepenseParMois($annee,$idmois);
        $data['RecetteMois']=$this->Admin_Model->getRecetteParMois($annee,$idmois);
        $data['page']="DepenseMensuelle";
        $this->load->view('AccAdmin',$data);
    }
    public function recetteMois(){
        $annee=isset($_GET['annee']) ? $_GET['annee'] : "";
        $this->load->model('Admin_Model');
        $data=array();
        $data['RecetteMois']=$this->Admin_Model->getRecetteParMois($annee);
        $data['page']="RecetteMensuelle";
        $this->load->view('AccAdmin',$data);
    }

}
?>