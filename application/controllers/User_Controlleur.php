<?php 	
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Controlleur extends CI_Controller{
	public function __construct(){
        parent::__construct();
    }

    public function depense(){
        $data=array();
        $this->load->model('Mon_Model');
        $data['ListeTypeDepense']=$this->Mon_Model->getAll("V_typedepense");
        $data['ListeMois']=$this->Mon_Model->getAll("mois");
        $idtypedepense=isset($_GET['idtypedepense']) ? $_GET['idtypedepense'] : ""; 
        $this->load->model('User_Model');       
        $data['ListeDepense']=$this->User_Model->getDepense($idtypedepense);
        $data['page']="ListeDepense";
        $this->load->view('AccUser',$data);     
    }
    public function addDepense(){
        $this->load->model('User_Model');
        $idtypedepense=$_GET['idtypedepense'];
        $jour=$_GET['jour'];
        $annee=$_GET['annee'];
        $montantdepense=$_GET['montantdepense'];
        if (isset($_GET['mois'])) {
            // Les cases à cocher ont été cochées
            $valeursSelectionnees = $_GET['mois'];
            foreach ($valeursSelectionnees as $valeur) {
                if (checkdate($valeur, $jour, $annee)) {
                    $datedepense=$annee."-".$valeur."-".$jour;
                    $this->User_Model->addDepense($idtypedepense,$datedepense,$montantdepense);  
                } else {
                    echo "La date n'existe pas: ".$jour."-".$valeur."-".$annee;
                }             
            }
        }
        $this->depense();
    }
    public function csvToBase(){
        $this->load->model('User_Model');
        $fileName = $_FILES["file"]["tmp_name"];
        $file = fopen($fileName, "r");
        try{
            $this->User_Model->csvToBase($file);
            $this->depense();
        }
        catch(Exception $e){
            echo $e->getMessage();
            $this->depense();
        }
    }
    public function updateDepense(){
        $idtypedepense=$_GET['idtypedepense'];
        $datedepense=$_GET['datedepense'];
        $montantdepense=$_GET['montantdepense'];
        $iddepense=$_GET['iddepense'];
        $this->load->model('User_Model');
        $this->User_Model->updateDepense($idtypedepense,$datedepense,$montantdepense,$iddepense);
        $this->depense();
    }
    public function deleteDepense(){
        $iddepense=$_GET['iddepense'];
        $this->load->model('User_Model');
        $this->User_Model->deleteDepense($iddepense);
        $this->depense();
    }

    public function Recette(){
        $data=array();
        $this->load->model('Mon_Model');
        $data['ListeTypeActe']=$this->Mon_Model->getAll("V_typeacte");
        $data['ListePatient']=$this->Mon_Model->getAll("V_patient");
        $idtypeacte=isset($_GET['idtypeacte']) ? $_GET['idtypeacte'] : ""; 
        $nompatient=isset($_GET['nompatient']) ? $_GET['nompatient'] : ""; 
        $this->load->model('User_Model');       
        $data['ListeRecette']=$this->User_Model->getRecette($idtypeacte,$nompatient);
        $data['page']="ListeRecette";
        $this->load->view('AccUser',$data); 
    }
    public function addRecette(){
        $idtypeacte=$_GET['idtypeacte'];
        $daterecette=$_GET['daterecette'];
        $montantrecette=$_GET['montantrecette'];
        $idpatient=$_GET['idpatient'];
        $this->load->model('User_Model');
        $this->User_Model->addRecette($idtypeacte,$daterecette,$idpatient,$montantrecette);
        $this->Recette();
    }
    public function updateRecette(){
        $idtypeacte=$_GET['idtypeacte'];
        $daterecette=$_GET['daterecette'];
        $montantrecette=$_GET['montantrecette'];
        $idpatient=$_GET['idpatient'];
        $idrecette=$_GET['idrecette'];
        $this->load->model('User_Model');
        $this->User_Model->updateRecette($idtypeacte,$daterecette,$montantrecette,$idpatient,$idrecette);
        $this->Recette();
    }
    public function deleteRecette(){
        $idrecette=$_GET['idrecette'];
        $this->load->model('User_Model');
        $this->User_Model->deleteRecette($idrecette);
        $this->Recette();
    }

    public function Acte(){
        $data=array();
        $this->load->model('Mon_Model');
        $data['ListePatient']=$this->Mon_Model->getAll("V_patient");
        $idtypeacte=isset($_GET['idtypeacte']) ? $_GET['idtypeacte'] : ""; 
        $nompatient=isset($_GET['nompatient']) ? $_GET['nompatient'] : ""; 
        $this->load->model('User_Model');       
        $data['ListeActe']=$this->User_Model->getActe($idtypeacte,$nompatient);
        $data['page']="ListeActe";
        $this->load->view('AccUser',$data); 
    }
    public function addActe(){
        $dateacte=$_GET['dateacte'];
        $idpatient=$_GET['idpatient'];
        $this->load->model('User_Model');
        $this->User_Model->addActe($dateacte,$idpatient);
        $this->Acte();
    }
    public function updateActe(){
        $dateacte=$_GET['dateacte'];
        $idpatient=$_GET['idpatient'];
        $idacte=$_GET['idacte'];
        $this->load->model('User_Model');
        $this->User_Model->updateActe($dateacte,$idpatient,$idacte);
        $this->Acte();
    }
    public function deleteActe(){
        $idacte=$_GET['idacte'];
        $this->load->model('User_Model');
        $this->User_Model->deleteActe($idacte);
        $this->Acte();
    }
    public function detailActe($idActe=''){
        $data=array();
        $idtypeacte=isset($_GET['idtypeacte']) ? $_GET['idtypeacte'] : ""; 
        $idacte=isset($_GET['idacte']) ? $_GET['idacte'] : $idActe; 
        $this->load->model('Mon_Model');
        $data['ListeTypeActe']=$this->Mon_Model->getAll("V_typeacte");
        $this->load->model('User_Model');
        $data['ListeDetailActe']=$this->User_Model->getDetailActeById($idtypeacte,$idacte);
        $liste=$this->User_Model->verifFacture($idacte);
        $existe=0;
        if(count($liste)==0){
            $existe=0;
        }
        else{
            $existe=1;
        }
        $data['idacte']=$idacte;
        $data['existe']=$existe;
        $data['page']="DetailActe";
        $this->load->view('AccUser',$data); 
    }
    public function addDetailActe(){
        $idacte=$_GET['idacte'];
        $idtypeacte=$_GET['idtypeacte'];
        $montantacte=$_GET['montantacte'];
        $datedetail=$_GET['datedetail'];
        $this->load->model('User_Model');
        $this->User_Model->addDetailActe($idacte,$idtypeacte,$montantacte,$datedetail);
        $this->DetailActe($idacte);
    }
    public function updateDetailActe(){
        $idacte=$_GET['idacte'];
        $idtypeacte=$_GET['idtypeacte'];
        $montantacte=$_GET['montantacte'];
        $datedetail=$_GET['datedetail'];
        $iddetailacte=$_GET['iddetailacte'];
        $this->load->model('User_Model');
        $this->User_Model->updateDetailActe($idacte,$idtypeacte,$montantacte,$datedetail,$iddetailacte);
        $this->DetailActe($idacte);
    }
    public function deleteDetailActe(){
        $iddetailacte=$_GET['iddetailacte'];
        $idacte=$_GET['idacte'];
        $this->load->model('User_Model');
        $this->User_Model->deleteDetailActe($iddetailacte);
        $this->DetailActe($idacte);
    }
    public function Facture(){
        $idacte=$_GET['idacte'];
        $datefacture=$_GET['datefacture'];
        $this->load->model('User_Model');
        $this->User_Model->addActeFacturer($idacte,$datefacture);
        $facture=$this->User_Model->getFactureByActe($idacte);
        $acte=$this->User_Model->getActeByActe($idacte);
        $detailacte=$this->User_Model->getDetailActeById("",$idacte);
        $this->load->model('Facture_Model');
        $this->Facture_Model->getPdfFacture($acte[0]['nom'],$acte[0]['age'],$facture[0]['idactefacturer'],$facture[0]['datefacture'],$detailacte);       
    }

    public function apercuFacture(){
        $idacte=$_GET['idacte'];
        $this->load->model('User_Model');
        $facture=$this->User_Model->getFactureByActe($idacte);
        $acte=$this->User_Model->getActeByActe($idacte);
        $detailacte=$this->User_Model->getDetailActeById("",$idacte);
        $this->load->model('Facture_Model');
        $this->Facture_Model->getPdfFacture($acte[0]['nom'],$acte[0]['age'],$facture[0]['idactefacturer'],$facture[0]['datefacture'],$detailacte);       

    }
}
?>