<?php 	
defined('BASEPATH') OR exit('No direct script access allowed');

class Mon_Controlleur extends CI_Controller{
	public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        $this->load->view('Login');
    }
    public function login(){
        $email=$_GET['email'];
        $mdp=$_GET['mdp'];
        $data=array();
        $this->load->model('Mon_Model');
        $user=$this->Mon_Model->login($email,$mdp);
        if(count($user)!=0){
            $data['page']="page";
            $_SESSION['idUtilisateur']=$user[0]['idutilisateur'];
            $statut=$user[0]['statut'];
            if($statut=="0"){
                $this->load->view('AccAdmin',$data);
            }
            else{
                $this->load->view('AccUser',$data);
            }
            
        }
        else{
            $data['erreur']="Identifiant non trouvé";
            $data['email']=$email;
            $this->load->view('Login',$data);
        }
    }
    public function deconnexion()
    {
        $_SESSION['idutilisateur']=null;
        $this->load->view('Login');
    }

}
?>
