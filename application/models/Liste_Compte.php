<?php
require('fpdf.php');

class Liste_Compte extends FPDF
{


	// Tableau simple
	function BasicTable($data,$taille)
	{
		// En-t�te
		$this->setFont('Arial','B',12);
		foreach($data[0] as $col)
			$this->Cell($taille,7,$col,1);
		$this->Ln();
		// Donn�es
		$this->setFont('Times','',11);
		for($i=1;$i<count($data);$i++)
		{
			foreach($data[$i] as $col)
				$this->Cell($taille,6,$col,1);
			$this->Ln();
		}
	}
}
?>
