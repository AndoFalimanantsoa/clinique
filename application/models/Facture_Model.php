<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
include('Liste_Compte.php');
include('ChiffresEnLettres.php');
class Facture_Model extends CI_Model{
    public function lettre($montant){
		$montant=sprintf("%.2f",$montant);
		$chiffre1=explode(".",$montant);
		$lettre=new ChiffreEnLettre();
		$avantVirgule=ucfirst($lettre->Conversion($chiffre1[0]));
		if($chiffre1[1]==0){
			$apresVirgule="";
		}
		else{
			$apresVirgule=$lettre->Conversion($chiffre1[1]);
		}
		$lettre=$avantVirgule." Ariary ".$apresVirgule;
		return $lettre;			
	}
    public function getPdfFacture($nom,$age,$numfacture,$datefacture,$detailfacture){
	    $pdf = new Liste_Compte();
		$pdf->AddPage();
        $pdf->setFont('Arial','B',12);
		$pdf->Cell(200, 0,"Clinique Mahasoa",0,1,'L');
        $pdf->setFont('Times','',10);
        $pdf->Cell(0,10,"Tel: 034 12 345 67",0,2,'L');       
        $pdf->Cell(50,0,"mahasoa@gmail.com",0,2,'L'); 
        $pdf->Cell(100, 12,"",0,2,'L');
        $pdf->Cell(100, 0,"Nom: ".$nom,0,2,'L');  
        $pdf->Cell(100, 10,"Age: ".$age."ans",0,2,'L');    
		$pdf->setFont('Arial','B',12);
		$pdf->Cell(200,10,"Facture N-".$numfacture,0,1,'C');
        $pdf->setFont('Times','',11);
        date_default_timezone_set("Asia/Bangkok");
		$date=date_create($datefacture);
		$daty=date_format($date,"d F Y");
        $pdf->Cell(200,0,"Date de facturation: ".$daty,0,2,'C');
        $pdf->Cell(100, 12,"",0,2,'L');
      //  $liste = $this->getVenteGlobalParMois($annee);
        $data = array();
		$data[0][0]='Acte';
        $data[0][1]='Montant(Ar)';
        $pdf->SetFont('Arial','B',14);
        $i=1;
        $total=0;
        foreach($detailfacture as $detail){
            $total=$total+$detail['montantacte'];
			$data[$i][0]=$detail['nomtypeacte'];
            $data[$i][1]=$detail['montantacte'];
            $i=$i+1;
        }
        $taille=count($detailfacture)+1;
        $data[$taille][0]='Total';
        $data[$taille][1]=sprintf("%.2f",$total);
        $pdf->SetFont('Arial','',14);
        $pdf->BasicTable($data,95);
        $pdf->Cell(100, 5,"",0,2,'L');
        $pdf->setFont('Times','U',10);
        $pdf->Cell(30, 10,"Montant en lettre:",0,0,'L'); 
        $pdf->setFont('Times','',11);
        $lettre=$this->lettre($total);
        $pdf->Cell(0, 10,$lettre,0,2,'L'); 
        $pdf->Output();
    }
}
?>