<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_Model extends CI_Model{
    public function addTypeActe($nomtypeacte,$code,$budget){
		$budgetmensuel=$budget/12;
		$sql="INSERT INTO TypeActe(nomtypeacte,codeacte,budgetacteannuel,budgetactemensuel) VALUES ('%s','%s','%s','%s')";
		$sql=sprintf($sql,$nomtypeacte,$code,$budget,$budgetmensuel);
	    $sql=$this->db->query($sql);
	}
    public function updateTypeActe($nomtypeacte,$code,$budget,$idtypeacte){
		$budgetmensuel=$budget/12;
        $sql="UPDATE TypeActe SET nomtypeacte='%s',codeacte='%s',budgetacteannuel='%s',budgetactemensuel='%s' where idtypeacte='%s'";
		$sql=sprintf($sql,$nomtypeacte,$code,$budget,$budgetmensuel,$idtypeacte);
	    $sql=$this->db->query($sql);	
	}
    public function deleteTypeActe($idtypeacte){
		$sql="INSERT INTO TypeActeNiala(idtypeacte) VALUES ('%s')";
		$sql=sprintf($sql,$idtypeacte);
	    $sql=$this->db->query($sql);
	}

    public function addTypeDepense($nomtypedepense,$code,$budget){
		$budgetmensuel=$budget/12;
		$sql="INSERT INTO TypeDepense(nomtypedepense,codedepense,budgetdepenseannuel,budgetdepensemensuel) VALUES ('%s','%s','%s','%s')";
		$sql=sprintf($sql,$nomtypedepense,$code,$budget,$budgetmensuel);
	    $sql=$this->db->query($sql);
	}
    public function updateTypeDepense($nomtypedepense,$code,$budget,$idtypedepense){
		$budgetmensuel=$budget/12;
        $sql="UPDATE TypeDepense SET nomtypedepense='%s',codedepense='%s',budgetdepenseannuel='%s',budgetdepensemensuel='%s' where idtypedepense='%s'";
		$sql=sprintf($sql,$nomtypedepense,$code,$budget,$budgetmensuel,$idtypedepense);
	    $sql=$this->db->query($sql);	
	}
    public function deleteTypeDepense($idtypedepense){
		$sql="INSERT INTO TypeDepenseNiala(idtypedepense) VALUES ('%s')";
		$sql=sprintf($sql,$idtypedepense);
	    $sql=$this->db->query($sql);
	}

    public function getPatient($nom,$age,$idgenre,$remboursement){
        $sql="SELECT * FROM V_Patient where ";
        if($nom!=""){
            $sql=$sql."nom ilike '%".$nom."%' and ";
        }
        if($age!=""){
            $sql=$sql."age<=".$age." and ";
        }
        if($idgenre!=""){
            $sql=$sql."idgenre='".$idgenre."' and ";
        }
        if($remboursement!=""){
            $sql=$sql."remboursement='".$remboursement."' and ";
        }
        $sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;
    }
    public function addPatient($nom,$datenaissance,$idgenre,$remboursement){
		$sql="INSERT INTO Patient(nom,datenaissance,idgenre,remboursement) VALUES ('%s','%s','%s','%s')";
		$sql=sprintf($sql,$nom,$datenaissance,$idgenre,$remboursement);
	    $sql=$this->db->query($sql);
	}
    public function updatePatient($nom,$datenaissance,$idgenre,$remboursement,$idpatient){
        $sql="UPDATE Patient SET nom='%s',datenaissance='%s',idgenre='%s',remboursement='%s' where idpatient='%s'";
		$sql=sprintf($sql,$nom,$datenaissance,$idgenre,$remboursement,$idpatient);
	    $sql=$this->db->query($sql);	
	}
    public function deletePatient($idpatient){
		$sql="INSERT INTO PatientNiala(idpatient) VALUES ('%s')";
		$sql=sprintf($sql,$idpatient);
	    $sql=$this->db->query($sql);
	}

    public function getDepenseParMois($annee,$idmois){
        $sql="select*from v_depenseParMois where";
		if($annee!=""){
			$sql=$sql." annee='".$annee."' and ";
		}
		if($idmois!=""){
			$sql=$sql." idmois='".$idmois."' and ";
		}
		$sql=$sql." 1=1";
		$query=$this->db->query($sql);
		$depense=array();
		foreach ($query->result_array() as $row) {
			$depense[]=$row;
		}
		return $depense;	
    }
	public function getRecetteParMois($annee,$idmois){
        $sql="select*from v_detailParMois where";
		if($annee!=""){
			$sql=$sql." annee='".$annee."' and ";
		}
		if($idmois!=""){
			$sql=$sql." idmois='".$idmois."' and ";
		}
		$sql=$sql." 1=1";
		$query=$this->db->query($sql);
		$recette=array();
		foreach ($query->result_array() as $row) {
			$recette[]=$row;
		}
		return $recette;	
    }
}
?>