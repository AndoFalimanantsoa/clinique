<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_Model extends CI_Model{
    public function getDepense($idtypedepense){
        $sql="SELECT * FROM V_depense where ";
        if($idtypedepense!=""){
            $sql=$sql."idtypedepense='".$idtypedepense."' and ";
        }
        $sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;
    }
    public function addDepense($idtypedepense,$datedepense,$montantdepense){
		$sql="INSERT INTO Depense(idtypedepense,datedepense,montantdepense) VALUES ('%s','%s','%s')";
		$sql=sprintf($sql,$idtypedepense,$datedepense,$montantdepense);
	    $sql=$this->db->query($sql);
	}

	public function getIdTypeDepenseByCode($code){
		$query=$this->db->query("SELECT * FROM TypeDepense where codedepense='".$code."'");
		$row=$query->row_array();
		$idtypedepense=$row['idtypedepense'];
		return $idtypedepense;
	}
	public function csvToBase($csv){
		try{
			 $column = fgetcsv($csv, 10000, ";");
			 while (($column = fgetcsv($csv, 10000, ";")) !== FALSE) {
				 $sql = "INSERT into Depense (datedepense,idtypedepense,montantdepense) values ('%s','%s','%s')";
				 $idtypedepense=$this->getIdTypeDepenseByCode($column[1]);
				 $sql=sprintf($sql,$column[0],$idtypedepense,$column[2]);
				 $result = $this->db->query($sql);
				 if (! empty($result)) {
					 $type = "success";
					 $message = "Les Données sont importées dans la base de données";
				 } else {
					 throw new Exception("Probleme dans l'importation du csv");
				 }
			 }
		}
		catch(Exception $e){
			throw $e;
		}
		}
    public function updateDepense($idtypedepense,$datedepense,$montantdepense,$iddepense){
        $sql="UPDATE Depense SET idtypedepense='%s',datedepense='%s',montantdepense='%s' where iddepense='%s'";
		$sql=sprintf($sql,$idtypedepense,$datedepense,$montantdepense,$iddepense);
	    $sql=$this->db->query($sql);	
	}
    public function deleteDepense($iddepense){
		$sql="INSERT INTO DepenseNiala(iddepense) VALUES ('%s')";
		$sql=sprintf($sql,$iddepense);
	    $sql=$this->db->query($sql);
	}

    public function getRecette($idtypeacte,$nompatient){
        $sql="SELECT * FROM V_recette where ";
        if($idtypeacte!=""){
            $sql=$sql."idtypeacte='".$idtypeacte."' and ";
        }
        if($nompatient!=""){
            $sql=$sql."nom ilike '%".$nompatient."%' and ";
        }
        $sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;
    }
    public function addRecette($idtypeacte,$daterecette,$idpatient,$montantrecette){
		$sql="INSERT INTO Recette(idtypeacte,daterecette,idpatient,montantrecette) VALUES ('%s','%s','%s','%s')";
		$sql=sprintf($sql,$idtypeacte,$daterecette,$idpatient,$montantrecette);
	    $sql=$this->db->query($sql);
	}
    public function updateRecette($idtypeacte,$daterecette,$montantrecette,$idpatient,$idrecette){
        $sql="UPDATE Recette SET idtypeacte='%s',daterecette='%s',montantrecette='%s',idpatient='%s' where idrecette='%s'";
		$sql=sprintf($sql,$idtypeacte,$daterecette,$montantrecette,$idpatient,$idrecette);
	    $sql=$this->db->query($sql);	
	}
    public function deleteRecette($idrecette){
		$sql="INSERT INTO RecetteNiala(idrecette) VALUES ('%s')";
		$sql=sprintf($sql,$idrecette);
	    $sql=$this->db->query($sql);
	}

	public function getActe($nompatient){
        $sql="SELECT * FROM V_acte where ";
        if($nompatient!=""){
            $sql=$sql."nom ilike '%".$nompatient."%' and ";
        }
        $sql=$sql."1=1 ";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;
    }
	public function addActe($dateacte,$idpatient){
		$sql="INSERT INTO Acte(dateacte,idpatient) VALUES ('%s','%s')";
		$sql=sprintf($sql,$dateacte,$idpatient);
	    $sql=$this->db->query($sql);
	}
	public function updateActe($dateacte,$idpatient,$idacte){
        $sql="UPDATE Acte SET dateacte='%s',idpatient='%s' where idacte='%s'";
		$sql=sprintf($sql,$dateacte,$idpatient,$idacte);
	    $sql=$this->db->query($sql);	
	}
    public function deleteActe($idacte){
		$sql="INSERT INTO ActeNiala(idacte) VALUES ('%s')";
		$sql=sprintf($sql,$idacte);
	    $sql=$this->db->query($sql);
	}

	public function getDetailActeById($idtypeacte,$idacte){
        $sql="SELECT * FROM V_detailacte where ";
        if($idtypeacte!=""){
            $sql=$sql."idtypeacte='".$idtypeacte."' and ";
        }
        $sql=$sql."1=1 and idacte='".$idacte."'";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;		
	}
	public function addDetailActe($idacte,$idtypeacte,$montantacte,$datedetail){
		$sql="INSERT INTO DetailActe(idacte,idtypeacte,montantacte,datedetail) VALUES ('%s','%s','%s','%s')";
		$sql=sprintf($sql,$idacte,$idtypeacte,$montantacte,$datedetail);
	    $sql=$this->db->query($sql);
	}
	public function updateDetailActe($idacte,$idtypeacte,$montantacte,$datedetail,$iddetailacte){
        $sql="UPDATE DetailActe SET idacte='%s',idtypeacte='%s',montantacte='%s',datedetail='%s' where iddetailacte='%s'";
		$sql=sprintf($sql,$idacte,$idtypeacte,$montantacte,$datedetail,$iddetailacte);
	    $sql=$this->db->query($sql);	
	}
    public function deleteDetailActe($iddetailacte){
		$sql="INSERT INTO DetailActeNiala(iddetailacte) VALUES ('%s')";
		$sql=sprintf($sql,$iddetailacte);
	    $sql=$this->db->query($sql);
	}

	public function addActeFacturer($idacte,$datefacture){
		$sql="INSERT INTO ActeFacturer(idacte,datefacture) VALUES ('%s','%s')";
		$sql=sprintf($sql,$idacte,$datefacture);
	    $sql=$this->db->query($sql);
	}
	public function getFactureByActe($idacte){
        $sql="SELECT * FROM actefacturer where idacte='".$idacte."'";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;		
	}
	public function getActeByActe($idacte){
        $sql="SELECT * FROM v_acte where idacte='".$idacte."'";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;		
	}

	public function verifFacture($idacte){
        $sql="SELECT * FROM acteFacturer where idacte='".$idacte."'";
		$query=$this->db->query($sql);
		$resultat=array();
		foreach ($query->result_array() as $row) {
			$resultat[]=$row;
		}
		return $resultat;			
	}
}
?>